<?php
session_start();
if (!isset($_SESSION['email']) && isset($_COOKIE['email'])) {

    $_SESSION['email'] = $_COOKIE['email'];
    $_SESSION['password'] = $_COOKIE['password'];
    $_SESSION['nome'] = $_COOKIE['nome'];
    $_SESSION['cognome'] = $_COOKIE['cognome'];
    $_SESSION['matricola'] = $_COOKIE['matricola'];
    $_SESSION['scuola'] = $_COOKIE['scuola'];
    $_SESSION['corso'] = $_COOKIE['corso'];
    $_SESSION['curriculum'] = $_COOKIE['curriculum'];
} elseif(isset($_SESSION['email']) && $_SESSION['email'] != NULL && isset($_SESSION['password'])
    && $_SESSION['password'] != NULL && $_SESSION['remember'] == 'no' && isset($_COOKIE['email'])) {

    $email = $_SESSION['email'];
    $password = $_SESSION['password'];
    setcookie("email", $email, time() - 14000);
    setcookie("password", $password, time() - 14000);
    setcookie("nome", $_SESSION['nome'], time() - 14000);
    setcookie("cognome", $_SESSION['cognome'], time() - 14000);
    setcookie("matricola", $_SESSION['matricola'], time() - 14000);
    setcookie("scuola", $_SESSION['scuola'], time() - 14000);
    setcookie("corso", $_SESSION['corso'], time() - 14000);
    setcookie("curriculum", $_SESSION['curriculum'], time() - 14000);
} elseif(!isset($_SESSION['email']) && !isset($_COOKIE['email'])) {

    header("location: index.php");
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="res/favicon.ico"/>
    <title>Studenti Online 2.0</title>

    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="dist/css/stylesheet.css">
    <link href="dist/css/footer.css" rel="stylesheet">

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/metisMenu/metisMenu.min.js"></script>
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="dist/js/sb-admin-2.js"></script>
</head>
<body>
<div id="wrapper">
    <nav id="UniboHeader" class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a id="UniboLogo" href="home.php"><img src="res/LogoUnibo.png" alt="Logo Unibo"></a>
        </div>
        <ul id="iconsNav" class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <?php
                include("database_connection.php");
                $stmt = $mysqli->prepare("SELECT * FROM notifica WHERE (matricolaDestinatario = ?) AND (letto = 0)");
                $stmt->bind_param("i", $_SESSION["matricola"]);
                $stmt->execute();

                $result = $stmt->get_result();
                print '    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <em class="fa fa-bell fa-fw"></em> <em class="notcounter">'.$result->num_rows.'</em> <em class="fa fa-caret-down"></em> 
                               </a>
                            <ul class="dropdown-menu dropdown-messages">';
                if ($result->num_rows > 0) {
                    $i = 0;
                    while ($row = $result->fetch_array()) {
                        print '<li class="bg-danger">
                            <a href="tables.php">
                                <div>
                                    <strong>'.$row['mittente'].'</strong>
                                    <span class="pull-right text-muted">
                            <em>'.$row['data'].'</em>
                          </span>
                                </div>
                                <div>'.$row['nota'].'</div>
                            </a>
                        </li>
                        <li class="divider"></li>';
                        $i = $i + 1;
                        if($i >= 5) break;
                    }
                }


                $stmt = $mysqli->prepare("SELECT codCorso FROM partecipazione WHERE (matricola = ?)");
                $stmt->bind_param("i", $_SESSION["matricola"]);
                $stmt->execute();

                $result = $stmt->get_result();
                $arrayCorsi = [];
                $arrayMaterie = [];
                if ($result->num_rows > 0) {
                    $i = 0;

                    while ($row = $result->fetch_array()) {
                        $arrayCorsi[$i] = (int)$row['codCorso'];
                        $i = $i + 1;
                    };
                    for ($j = 0; $j < $i; $j++) {
                        $stmt = $mysqli->prepare("SELECT * FROM corsoDiStudio WHERE (codCorso = ?)");
                        $stmt->bind_param("i", $arrayCorsi[$j]);
                        $stmt->execute();

                        $result = $stmt->get_result();
                        while ($row = $result->fetch_array()) {
                            $arrayMaterie[] = $row;
                        }
                    }
                }
                ?>
            <li>
                <a class="text-center" href="tables.php">
                    <strong>Vedi tutte le notifiche</strong>
                    <em class="fa fa-angle-right"></em> 
                </a>
            </li>
        </ul>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <?php
                print $_SESSION['nome']." ".$_SESSION['cognome'];
                ?>
                <em class="fa fa-user fa-fw"></em>  <em class="fa fa-caret-down"></em> 
            </a>
            <ul class="dropdown-menu dropdown-tasks">
                <li><a href="profile.php"><em class="fa fa-user fa-fw"></em>  User Profile</a>
                </li>
                <li><a href="#"><em class="fa fa-gear fa-fw"></em>  Settings</a>
                </li>
                <li class="divider"></li>
                <li><a href="index.php"><em class="fa fa-sign-out fa-fw"></em>  Logout</a>
                </li>
            </ul>
        </li>
        </ul>

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <label for="ricerca" hidden>Ricerca</label>
                            <input id="ricerca" type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                      <span class="fa fa-search"></span>
                    </button>
                  </span>
                        </div>
                    </li>
                    <li>
                        <a class="active" href="home.php"><span class="fa fa-home fa-fw"></span> Home</a>
                    </li>
                    <li>
                        <a href=""><span class="fa fa-bar-chart-o fa-fw"></span> Insegnamenti<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="materiale.php"> Corsi di Studio</a>
                            </li>
                            <li>
                                <a href="esami.php"> Iscrizione Esami</a>
                            </li>
                            <li>
                                <a href="libretto.php"> Libretto Voti</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href=""><span class="fa fa-table fa-fw"></span> Gestione Studi<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="http://www.unibo.it/it/didattica/iscrizioni-trasferimenti-e-laurea"> Immatricolazione</a>
                            </li>
                            <li>
                                <a href="http://www.unibo.it/it/didattica/iscrizioni-trasferimenti-e-laurea/prova-finale-e-garanzia-di-originalita/domanda-laurea-scadenze-e-tasse"> Laurea</a>
                            </li>
                            <li>
                                <a href="http://www.unibo.it/it/didattica/iscrizioni-trasferimenti-e-laurea"> Passaggio di corso</a>
                            </li>
                            <li>
                                <a href="http://www.unibo.it/it/didattica/iscrizioni-trasferimenti-e-laurea/trasferirsi-ad-un-altro-ateneo/trasferirsi-ad-un-altro-ateneo"> Trasferimento</a>
                            </li>
                            <li>
                                <a href="http://www.unibo.it/it/didattica/iscrizioni-trasferimenti-e-laurea"> Rinuncia agli studi</a>
                            </li>
                            <li>
                                <a href="http://www.unibo.it/it/didattica/iscrizioni-trasferimenti-e-laurea"> Sospendi gli studi</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href=""><span class="fa fa-edit fa-fw"></span> Servizi Amministrativi<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="http://www.unibo.it/it/didattica/iscrizioni-trasferimenti-e-laurea/bandi-per-prove-di-ammissione"> Bandi</a>
                            </li>
                            <li>
                                <a href="http://www.unibo.it/it/servizi-e-opportunita/certificati-e-richiesta-duplicati/certificati-autocertificazioni-e-diploma-supplement"> Certificazioni</a>
                            </li>
                            <li>
                                <a href="http://www.unibo.it/it/didattica/iscrizioni-trasferimenti-e-laurea/tasse-e-contributi"> Situazione Tasse</a>
                            </li>
                            <li>
                                <a href="http://www.unibo.it/it/servizi-e-opportunita/servizi-online/servizi-online-per-studenti-1/guida-servizi-online-studenti/piani-di-studio-online"> Piani di Studio</a>
                            </li>
                            <li>
                                <a href="http://www.unibo.it/it/didattica/iscrizioni-trasferimenti-e-laurea/bandi-per-prove-di-ammissione"> Esami di Ammissione</a>
                            </li>
                            <li>
                                <a href="http://www.cla.unibo.it/idoneita-linguistica"> Idoneità Linguistica</a>
                            </li>
                            <li>
                                <a href="http://www.unibo.it/it/internazionale/tirocini-estero/bando-erasmus-mobilita-tirocinio"> Mobilità Internazionale</a>
                            </li>
                            <li>
                                <a href="https://almaorienta.unibo.it/tirocini"> Area Tirocini</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href=""><span class="fa fa-wrench fa-fw"></span> Servizi<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="http://www.unibo.it/it/ateneo/concorsi-e-selezioni/personale-ta/mobilita-esterna-in-entrata"> Mobilità</a>
                            </li>
                            <li>
                                <a href="http://www.unibo.it/it/servizi-e-opportunita/borse-di-studio-e-agevolazioni/mense-a-bologna-e-campus/mense-e-punti-ristoro"> Mense e Associati</a>
                            </li>
                            <li>
                                <a href="http://www.unibo.it/it/servizi-e-opportunita/studio-e-non-solo/trasporti-e-mobilita/bici"> Bike-Sharing</a>
                            </li>
                            <li>
                                <a href="tables.php"> Gestione Notifiche</a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-divider"></li>
                    <li class="mails">
                        <a href="https://outlook.office365.com/studio.unibo.it" target="_blank" ><span class="fa fa-envelope fa-fw"></span> Mail</a>
                    </li>
                    <li class="prefs">
                        <a href="home.php"><em class="fa fa-star fa-fw"></em>  Preferiti</a>
                    </li>
                    <li>
                        <a href="profile.php"><span class="fa fa-sitemap fa-fw"></span> Profilo</a>
                    </li>
                </ul>
                <div class="well">
                    <h4 class="text-center">
                        <?php
                        print $_SESSION['nome']." ".$_SESSION['cognome'];
                        ?>
                    </h4>
                    <h4>
                        <small>Matricola:
                            <?php
                            print "0000".$_SESSION['matricola'];
                            ?>
                        </small>
                    </h4>
                    <h5>
                        Scuola:
                        <?php
                        print $_SESSION['scuola'];
                        ?>
                    </h5>
                    <h5>
                        Facoltà:
                        <?php
                        print $_SESSION['corso'];
                        ?>
                    </h5>
                    <h5>
                        Curriculum:
                        <?php
                        print $_SESSION['curriculum'];
                        ?>
                    </h5>
                </div>
            </div>
        </div>
    </nav>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header text-center">Esami</h2>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="footer-bottom">
        <div class="container">
            <h6 class="pull-left">Copyright 2016 - ALMA MATER STUDIORUM - <small>Università di Bologna Via Zamboni, 33 - 40126 Bologna - Partita IVA: 01131710376 Informativa sulla Privacy - </small></h6>
            <div class="pull-right">
                <ul class="nav nav-pills social">
                    <li><a href="https://www.facebook.com/unibo.it/"><em class="fa fa-facebook"></em> </a></li>
                    <li><a href="https://twitter.com/unibomagazine?lang=it"><em class="fa fa-twitter"></em> </a></li>
                    <li><a href="http://www.unibo.it/it"><em class="fa fa-university"></em> </a></li>
                    <li><a href="https://it.pinterest.com/"><em class="fa fa-pinterest"></em> </a></li>
                    <li><a href="https://www.youtube.com/user/UniBologna"><em class="fa fa-youtube"></em> </a></li>
                    <li>
                        <div id="google_translate_element"></div><script type="text/javascript">
                            function googleTranslateElementInit() {
                                new google.translate.TranslateElement({pageLanguage: 'it'}, 'google_translate_element');
                            }
                        </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
</body>
</html>