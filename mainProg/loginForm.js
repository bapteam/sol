$(function() {
    $("input[name='email'][type='email']").on({
        focusin: function() {
            $(this).css("border-color", "yellow");
        },
        focusout: function() {
            let value = $(this).val();
            let caratteri = /^([a-zA-Z0-9_.-])+\@(([a-zA-Z0-9_.-]))+\.+([a-zA-Z]{2,4})+$/;
            let res = value.match(caratteri);

            if(value.length < 5 || !res){
                $(this).css("border-color", "red");
            } else {
                $(this).removeAttr('style');
            }
        }
    });

    $("input[name='password']").on({
        focusin: function() {
            $(this).css("border-color", "yellow");
        },
        focusout: function() {
            $(this).removeAttr('style');
        }
    });
});
