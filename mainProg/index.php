<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="res/favicon.ico"/>
    <title>Studenti Online 2.0</title>

    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="dist/css/stylesheet.css">
    <link href="dist/css/footer.css" rel="stylesheet">

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/metisMenu/metisMenu.min.js"></script>
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="dist/js/sb-admin-2.js"></script>
      <script src="loginForm.js"></script>
  </head>
  <body>

    <nav id="UniboHeader" class="nav">
      <div>
        <a id="UniboLogo" href=""><img src="res/LogoUnibo.png" alt="Logo Unibo"></a>
      </div>
    </nav>
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <div class="login-panel panel panel-default">
            <div class="panel-heading" id="headingLogin">
              <h3 class="panel-title">Please Sign In</h3>
            </div>
              <?php
              session_start();
              if (isset($_SESSION['ErrorFlag']) && $_SESSION['ErrorFlag'] == true)  {
                  $message = "Riprova!, Email o Password errate";
                  echo "<script type='text/javascript'>alert('$message');</script>";
                  $_SESSION['ErrorFlag'] = false;
              }
              ?>
            <div class="panel-body">
                <form action="verifica.php" method="post">
                  <fieldset>
                    <div class="form-group">
                        <input id="loginE" class="form-control" placeholder="E-mail" name="email" type="email" autocomplete="on">
                    </div>
                    <div class="form-group">
                        <input id="loginP" class="form-control" placeholder="Password" name="password" type="password">
                        <div class="checkbox">
                            <label>
                                <input name="remember" type="checkbox" value="yes">Remember Me
                            </label>
                        </div>
                    </div>
                    <input type="submit" name="logIn" value="Log In" class="btn btn-lg btn-success btn-block">
                  </fieldset>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
