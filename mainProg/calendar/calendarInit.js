
$(document).ready(function() {

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listWeek'
        },
        locale: 'it',
        defaultDate: new Date(),
        editable: false,
        navLinks: true, // can click day/week names to navigate views
        eventLimit: true, // allow "more" link when too many events
        eventSources: [

            {
                url: './calendar/php/get-events.php',
                color: 'red',
                error: function () {
                    $('#script-warning').show();
                },
                className: 'events',
            },

            {
                url: './calendar/php/get-lessons.php',
                color: 'green',
                error: function () {
                    $('#script-warning').show();
                },
                className: 'lessons',
            },
            {
                url: './calendar/php/get-exams.php',
                color: 'blue',
                error: function () {
                    $('#script-warning').show();
                },
                className: 'exams',
            },
            {
                url: './calendar/php/get-taxes.php',
                color: 'grey',
                error: function () {
                    $('#script-warning').show();
                },
                className: 'taxes',
            }

        ],
        loading: function(bool) {
            $('#loading').toggle(bool);
        }
    });

});
