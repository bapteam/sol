<?php
require("../../database_connection.php");
$lessons = array();
$events = array();
$taxes = array();
$exams = array();


$stmt = $mysqli->prepare("SELECT * FROM eventi");
$stmt->execute();
$result = $stmt->get_result();


while($row=mysqli_fetch_array($result)) {

    if($row['tipologia'] == "evento"){

        if(isset($row['end'])){
            if(isset($row['url'])){
                $events[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start'], 'end' => $row['end'], 'url' => $row['url']);
            }
        }else if(isset($row['end'])){
            $events[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start'], 'end' => $row['end']);
        }else if(isset($row['url'])){
            $events[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start'], 'url' => $row['url']);
        }else if((!isset($row['url']) || $row['url'] == "") && (!isset($row['end'])|| $row['end'] == "")){
            $events[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start']);
        }

    }

    if($row['tipologia'] == "lezione"){

        if(isset($row['end'])){
            if(isset($row['url'])){
                $lessons[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start'], 'end' => $row['end'], 'url' => $row['url']);
            }
        }else if(isset($row['end'])){
            $lessons[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start'], 'end' => $row['end']);
        }else if(isset($row['url'])){
            $lessons[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start'], 'url' => $row['url']);
        }else if((!isset($row['url']) || $row['url'] == "") && (!isset($row['end'])|| $row['end'] == "")){
            $lessons[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start']);
        }

    }

    if($row['tipologia'] == "tassa"){

        if(isset($row['end'])){
            if(isset($row['url'])){
                $taxes[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start'], 'end' => $row['end'], 'url' => $row['url']);
            }
        }else if(isset($row['end'])){
            $taxes[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start'], 'end' => $row['end']);
        }else if(isset($row['url'])){
            $taxes[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start'], 'url' => $row['url']);
        }else if((!isset($row['url']) || $row['url'] == "") && (!isset($row['end'])|| $row['end'] == "")){
            $taxes[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start']);
        }

    }

    if($row['tipologia'] == 'esame'){

        if(isset($row['end'])){
            if(isset($row['url'])){
                $exams[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start'], 'end' => $row['end'], 'url' => $row['url']);
            }
        }else if(isset($row['end'])){
            $exams[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start'], 'end' => $row['end']);
        }else if(isset($row['url'])){
            $exams[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start'], 'url' => $row['url']);
        }else if((!isset($row['url']) || $row['url'] == "") && (!isset($row['end'])|| $row['end'] == "")){
            $exams[] = array('id' => $row['idevento'], 'title'=> $row['titolo'], 'start' => $row['start']);
        }

    }


}


$fp = fopen('../json/events.json', 'w');
fwrite($fp, json_encode($events));
fclose($fp);

$fp = fopen('../json/lessons.json', 'w');
fwrite($fp, json_encode($lessons));
fclose($fp);

$fp = fopen('../json/examsEv.json', 'w');
fwrite($fp, json_encode($exams));
fclose($fp);

$fp = fopen('../json/taxes.json', 'w');
fwrite($fp, json_encode($taxes));
fclose($fp);

?>