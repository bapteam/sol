<?php
require("../database_connection.php");
session_start();
$response = array();
$bookmarks = array();

$stmt = $mysqli->prepare("SELECT * FROM corsoDiStudio S, partecipazione I WHERE S.codCorso = I.codCorso AND preferiti = 'si' AND (matricola = ?)");
$stmt->bind_param("i", $_SESSION["matricola"]);
$stmt->execute();
$result = $stmt->get_result();


while($row=mysqli_fetch_array($result)) {
    $bookmarks[] = array('codice' => $row['codCorso'], 'nome'=> $row['nomeCorso'], 'sito' => $row['sitoCorso'], 'materiale' => $row['materiale'], 'elearning' => $row['e-learning']);
}

$response['bookmarks'] = $bookmarks;

$fp = fopen('bookmarks.json', 'w');
fwrite($fp, json_encode($response));
fclose($fp);

?>