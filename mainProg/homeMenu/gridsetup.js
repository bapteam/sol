$(function () {
    let i;
    var options = {
        width: 6,
        float: false,
        removeTimeout: 100,
        acceptWidgets: '.grid-stack-item'
    };
    $('#grid1').gridstack(_.defaults({
        float: false,
        resizable: {
            handles: 'no'
        }
    },options));

    var items1 = [];
    var temp = [];


    $.ajax({
        url: 'homeMenu/bookmarks.json',
        contentType: 'application/json',
        type: 'GET',
    })

        .done(function (data) {


            var oscpuInfo = window.navigator.oscpu || window.navigator.platform;
            if(oscpuInfo.substr(0, 3) == "Mac"){

                temp = JSON.parse(data).bookmarks;
            } else{
                temp = data.bookmarks;
            }

            for (i = 0; i < temp.length; i++) {
                items1.push( {x: i*5%10, y: 0, width: 3, height: 3, codice: temp[i].codice, nome: temp[i].nome, sito : temp[i].sito, materiale: temp[i].materiale, elearning: temp[i].elearning});
            }

            i = 0;

            $('#grid1').each(function () {
                var grid = $(this).data('gridstack');
                _.each(items1, function (node) {
                    grid.addWidget($('<div id="gridstackelement'+i+'"><div class="grid-stack-item-content"></div><div></div></div>'),
                        node.x, node.y, node.width, node.height);
                    document.getElementsByClassName("grid-stack-item-content")[i++].innerHTML = '<button class="btn btn-danger pull-right"><span class="fa fa-remove"></span></button>' +
                        '</br><div><p><br><b>' + node.nome + '</b><br>Sito Corso: <a href="http://' + node.sito + '">Link</a>' +
                    '<br>Materiale Corso: <a href="http://' + node.materiale +' ">Link</a>'+ '<br>E-learning: <a href="http://' + node.elearning +' ">Link</a>' +
                        '<br>Google Drive: <a href="#!">Link</a><br>Libri Utili: <a href="#!">Link</p></div>';
                }, this);
            });

            $('.sidebar .grid-stack-item').draggable({
                revert: 'invalid',
                handle: '.grid-stack-item-content',
                scroll: false,
                appendTo: 'body'
            });


            var visible = false;

            $( ".btn-danger" ).each(function(index) {
                $(this).on("click", function(){
                    $('.grid-stack').data('gridstack').removeWidget('#gridstackelement'+index);
                    $.ajax({
                        url: "./homeMenu/removeBookmarks.php",
                        type: "POST",
                        dataType:'json',
                        data: ({remove: items1[index].codice})
                    });
                });
            });


            new function () {
                this.showGrid2 = function () {
                    let i;
                    if(visible){
                        i = 0;
                        for(i = 0; i < document.getElementsByClassName("btn-danger").length; i++){
                            document.getElementsByClassName("btn-danger")[i].style.visibility = "hidden";
                        }
                        document.getElementById("show-grid").style.backgroundImage = "url('homeMenu/meno.png')";
                        visible = false;
                    }else{
                        i = 0;
                        for(i = 0; i < document.getElementsByClassName("btn-danger").length; i++){
                            document.getElementsByClassName("btn-danger")[i].style.visibility = "visible";
                        }
                        document.getElementById("show-grid").style.backgroundImage = "url('homeMenu/ics.png')";
                        visible = true;
                    }
                }.bind(this);

                $('#show-grid').click(this.showGrid2);
            };
        })


        .fail(function (data) {
            console.log("NESSUN PREFERITO");
        });

});


