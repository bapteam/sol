function setCookie(name,idcard,days) {
    var arr = new Array();
    var obj = new Object();

    //add new cookie data
    obj.name = name;
    obj.idcard = idcard;
    arr.push(obj);

    //get old cookie data
    var temp = getCookie();
    if (temp != null) {
        //concat new and old cookie data
        for (var i = 0; i < temp.length; i++) {
            var ob = new Object();
            ob.name = temp[i].name;
            ob.idcard = temp[i].idcard;
            arr.push(ob);
        }
    }
    var objWarp = new Object();
    objWarp.user = arr;
    var val = JSON.stringify(objWarp);

    //set cookie date expired
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = "; expires="+date.toGMTString();
    //create cookie
    document.cookie = "user_cookie="+val+expires;
}

function getCookie() {
    var key,val,res;
    //get all cookie
    var oldCookie = document.cookie.split(';');
    for (var i = 0; i < oldCookie.length; i++) {
        key = oldCookie[i].substr(0,oldCookie[i].indexOf("="));
        val = oldCookie[i].substr(oldCookie[i].indexOf("=")+1);
        key = key.replace(/^\s+|\s+$/g,"");
        //find "user_cookie"
        if(key == "user_cookie") {
            res = val;
        }
    }
    if (res == undefined) {
        return null;
    } else {
        res = JSON.parse(res);
        return res.user;
    }
}