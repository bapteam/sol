<?php
$_encryptMethod = "AES-256-CBC";
$_key = "Tecnologie Web";
$_iv = "TecWeb";
session_start();
if (isset($_POST['email']) && $_POST['email'] != null
    && isset($_POST['password']) && $_POST['password'] != null) {

    $remember = isset($_POST['remember']) ? $_POST['remember'] : 'no';
    include("database_connection.php");

    $email = $_POST['email'];
    $pw = $_POST['password'];

    $stmt = $mysqli->prepare("SELECT * FROM studenti WHERE (email = ?)");
    $stmt->bind_param("s", $email);
    $stmt->execute();

    $result = $stmt->get_result();
    if ($result->num_rows == 1) {
        $row = $result->fetch_assoc();
        if (encrypt($pw, $_key, $_iv, $_encryptMethod) == $row['password']) {
            $_SESSION['email'] = $email;
            $_SESSION['password'] = $pw;
            $_SESSION['remember'] = $remember;
            $_SESSION['nome'] = $row['nome'];
            $_SESSION['cognome'] = $row['cognome'];
            $_SESSION['matricola'] = $row['matricola'];
            $_SESSION['scuola'] = $row['scuola'];
            $_SESSION['corso'] = $row['corso'];
            $_SESSION['curriculum'] = $row['curriculum'];
            setcookie("email", $email, time() + 14000);
            setcookie("password", $password, time() + 14000);
            setcookie("nome", $_SESSION['nome'], time() + 14000);
            setcookie("cognome", $_SESSION['cognome'], time() + 14000);
            setcookie("matricola", $_SESSION['matricola'], time() + 14000);
            setcookie("scuola", $_SESSION['scuola'], time() + 14000);
            setcookie("corso", $_SESSION['corso'], time() + 14000);
            setcookie("curriculum", $_SESSION['curriculum'], time() + 14000);
            header("location: home.php");
        } else {
            $_SESSION['ErrorFlag'] = true;
            header("location: index.php");
        }
    } elseif ($result->num_rows != 1) {
        $_SESSION['ErrorFlag'] = true;
        header("location: index.php");
    }
} else {
    $_SESSION['ErrorFlag'] = true;
    header("location: index.php");
}

function encrypt($string,$_key,$_iv,$_encryptMethod)
{
    // hash
    $key = hash('sha256', $_key);
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $_iv), 0, 16);
    $output = openssl_encrypt($string, $_encryptMethod, $key, 0, $iv);
    $output = base64_encode($output);
    return $output;
}
?>